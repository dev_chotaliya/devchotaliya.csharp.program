﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.TestPolimorphism.Dynamic
{
    public class CarTest
    {
        public virtual string DisplayCarName()
        {
            return "Car";
        }

        public string DisplayColorName()
        {
            return "Red";
        }
    }

    public class TataCarTest : CarTest
    {
        public override string DisplayCarName()
        {
            return "TataCar";
        }

        public string DisplayColorName()
        {
            return "White";
        }
    }

    public class FordCarTest : CarTest
    {
        public override string DisplayCarName()
        {
            return "FordCar";
        }

        public string DisplayColorName()
        {
            return "Green";
        }
    }
}
