﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Inheritence
{
    public class GrandFather
    {
        public void DisplayGrandFather()
        {
            Console.WriteLine("Grandfather...");
        }
    }

    public class Father : GrandFather
    {
        public void DisplayFather()
        {
            Console.WriteLine("Father...");
        }
    }

    public class Son: Father
    {
        public void DisplaySon()
        {
            Console.WriteLine("Son...");
        }
    }
}
