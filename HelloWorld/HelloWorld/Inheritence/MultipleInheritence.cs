﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Inheritence
{
    interface IAdd
    {
        int add(int a, int b);
    }
    interface ISub
    {
        int sub(int x, int y);
    }
    interface IMul
    {
        int mul(int r, int s);
    }
    interface IDiv
    {
        int div(int c, int d);
    }
    public class Calculation : IAdd, ISub, IMul, IDiv
    {
        public int result1;
        public int add(int a, int b)
        {
            return result1 = a + b;
        }
        public int result2;
        public int sub(int x, int y)
        {
            return result2 = x - y;
        }
        public int result3;
        public int mul(int r, int s)
        {
            return result3 = r * s;
        }
        public int result4;
        public int div(int c, int d)
        {
            return result4 = c / d;
        }
    }
}
