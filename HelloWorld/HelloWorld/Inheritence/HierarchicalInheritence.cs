﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Inheritence
{
    public class Principal
    {
        public void Monitor()
        {
            Console.WriteLine("Monitor");
        }
    }
    public class Teacher : Principal
    {
        public void Teach()
        {
            Console.WriteLine("Teach");
        }
    }
    public class Student : Principal
    {
        public void Learn()
        {
            Console.WriteLine("Learn");
        }
    }
}
