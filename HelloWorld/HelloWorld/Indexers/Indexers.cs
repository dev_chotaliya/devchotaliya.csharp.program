﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Indexers
{
    public class Indexers
    {
        // Declare an array to store the data elements.
        private string[] names = new string[10];

        // Define the indexer to allow client code to use [] notation.
        public string this[int i]
        {
            get
            {
                return names[i];
            }
            set
            {
                names[i] = value;
            }
        }
    }
}
