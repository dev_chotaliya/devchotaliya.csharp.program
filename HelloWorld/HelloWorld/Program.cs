﻿using HelloWorld.Polimorphism.Static;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HelloWorld
{

    class Program
    {

        static void Main(string[] args)
        {

            #region "Hello World

            //Console.WriteLine("Hello World");
            //Console.ReadLine();

            #endregion

            #region "Console Input & Output"

            //Console.WriteLine("Enter the value of A:");
            //string A = Console.ReadLine();
            //Console.WriteLine("Enter the value of B:");
            //string B = Console.ReadLine();

            //int intA = Convert.ToInt32(A);
            //int intB = Convert.ToInt32(B);

            //int C = intA + intB;
            //Console.WriteLine("Total: " + C);
            //Console.ReadLine();

            #endregion

            #region "Access Modifier"

            //AccessModifier.PublicAccessModifier publicAccessModifier = new AccessModifier.PublicAccessModifier();
            //publicAccessModifier.Number1 = 1;

            //Console.WriteLine(publicAccessModifier.Number1);

            //AccessModifier.PrivateAccessModifier privateAccessModifier = new AccessModifier.PrivateAccessModifier();
            ////Private variable can't access here...

            //ProtectedAccessModifierDerived protectedAccessModifierDerived = new ProtectedAccessModifierDerived();
            ////Protected variable access into derived class...
            //Console.WriteLine(protectedAccessModifierDerived.Get());

            //AccessModifier.InternalAccessModifier internalAccessModifier = new AccessModifier.InternalAccessModifier();
            //internalAccessModifier.Number1 = 1;

            //Console.WriteLine(internalAccessModifier.Number1);


            #endregion

            #region "Decision Making Statement"

            //string str1, str2;

            //Console.WriteLine("Enter the value of String 1:");
            //str1 = Console.ReadLine();
            //Console.WriteLine("Enter the value of String 2:");
            //str2 = Console.ReadLine();

            //if (str1 == str2)
            //{
            //    Console.WriteLine("Both String are equal");
            //}
            //else
            //{
            //    Console.WriteLine("Both String are not equal");
            //}

            //string output = str1 == str2 ? "Both String are equal" : "Both String are not equal";

            //Console.WriteLine("Output using Ternary Operator:" + output);
            //Console.ReadLine();

            #endregion

            #region "Conditional Loops Statement"

            //Console.WriteLine("Enter the value of Counter:");
            //int counter = Convert.ToInt32(Console.ReadLine());

            //int i = 0;

            //for (; i <= counter; i++)
            //{
            //    Console.WriteLine(i);
            //}

            //int[] arrayInts = new int[5];

            //arrayInts[0] = 6;
            //arrayInts[1] = 7;
            //arrayInts[2] = 8;
            //arrayInts[3] = 9;
            //arrayInts[4] = 10;

            //foreach (int test in arrayInts)
            //{
            //    Console.WriteLine(test);
            //}

            //Console.ReadLine();

            #endregion

            #region "Constructor"

            //Constructor.DefaultConstructor defaultConstructor = new Constructor.DefaultConstructor();
            //Constructor.DefaultConstructor defaultParameterizedConstructor = new Constructor.DefaultConstructor("123");

            ////Constructor.PrivateConstructor privateConstructor = new Constructor.PrivateConstructor();

            //Constructor.StaticConstructor staticConstructor = new Constructor.StaticConstructor("123");

            //Console.ReadLine();

            #endregion

            #region "Polimorphism"

            //TestPolimorphism.Static.CalcTest calcTest = new TestPolimorphism.Static.CalcTest();
            //int result = calcTest.Add(10, 5);
            //Console.WriteLine("The output of two variable: " + result);

            //result = calcTest.Add(10, 5, 3);
            //Console.WriteLine($"The output of three variable: {result}");

            //result = calcTest.Add(10, "Test", 10);
            //Console.WriteLine($"The output of three variable with different datatype: {result}");



            //TestPolimorphism.Dynamic.BikeTest bikeTest = new TestPolimorphism.Dynamic.BikeTest();
            //Console.WriteLine(bikeTest.DisplayName());

            //TestPolimorphism.Dynamic.ActivaBikeTest activaBikeTest = new TestPolimorphism.Dynamic.ActivaBikeTest();
            //Console.WriteLine(activaBikeTest.DisplayName());

            //TestPolimorphism.Dynamic.CarTest carTest = new TestPolimorphism.Dynamic.CarTest();
            //Console.WriteLine(carTest.DisplayCarName());

            //TestPolimorphism.Dynamic.TataCarTest tataCarTest = new TestPolimorphism.Dynamic.TataCarTest();
            //Console.WriteLine(tataCarTest.DisplayCarName());

            //TestPolimorphism.Dynamic.CarTest carTest1 = new TestPolimorphism.Dynamic.FordCarTest();
            //Console.WriteLine(carTest1.DisplayCarName());

            //Console.ReadLine();

            #endregion

            #region "Abstraction"

            //TestAbstration.TestAreaClass testAreaClass = new TestAbstration.TestSquareClass();
            //Console.WriteLine(testAreaClass.Area(5));

            //Console.ReadLine();

            #endregion

            #region "Inteface"

            //TestInterface.ITestArea testArea = new TestInterface.TestArea();
            //Console.WriteLine(testArea.IArea(5));

            //Console.ReadLine();

            #endregion

            #region "Inheritence"

            //TestInheritence.TestShape shape = new TestInheritence.TestShape();
            //shape.width = 10;
            //shape.height = 8;
            //Console.WriteLine("Shape: " + shape.GetWidth());

            //TestInheritence.TestRectangle rectangle = new TestInheritence.TestRectangle();
            //rectangle.width = 10;
            //rectangle.height = 10;

            //Console.WriteLine("Rectangle: " + rectangle.GetArea());


            //TestInheritence.TestSon testSon = new TestInheritence.TestSon();
            //testSon.DisplayGrandFather();
            //testSon.DisplayFather();
            //testSon.DisplaySon();

            //TestInheritence.TestCalc calc = new TestInheritence.TestCalc(10, 5);
            //calc.Add();
            //Console.WriteLine("Sum: " + calc.DisplayResult());
            //calc.Sub();
            //Console.WriteLine("Sub: " + calc.DisplayResult()); 

            //TestInheritence.TestStudent student = new TestInheritence.TestStudent();
            //student.Monitor();
            //student.Learn();

            //TestInheritence.TestTeacher teacher = new TestInheritence.TestTeacher();
            //teacher.Monitor();
            //teacher.Teach();

            //TestInheritence.TestPrincipal principal = new TestInheritence.TestPrincipal();
            //principal.Monitor();

            //Console.ReadLine();


            #endregion

            #region "Property"

            TestProperty.TestProperty property = new TestProperty.TestProperty();
            property.Result = 19;
            property.Result1 = 20;
            int result = property.Result;

            Console.WriteLine(result);
            Console.WriteLine(property.Result1);

            Console.ReadLine();

            #endregion 

        }
    }
}
