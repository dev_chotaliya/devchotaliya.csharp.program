﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Property
{
    public class Property
    {
        private int x;
        public void SetX(int i)
        {
            x = i;
        }
        public int GetX()
        {
            return x;
        }


        #region Using Properties

        //private int x;
        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }

        #endregion
    }
}
