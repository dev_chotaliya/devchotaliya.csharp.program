﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Polimorphism.Static
{
    public class Calc
    {
        // adding two integer values.
        public int Add(int a, int b)
        {
            int sum = a + b;
            return sum;
        }

        // adding three integer values.
        public int Add(int a, int b, int c)
        {
            int sum = a + b + c;
            return sum;
        }

        #region Change Order Of Parameter


        // Method
        public void Identity(String name, int id)
        {

            Console.WriteLine("Name : " + name + ", "
                              + "Id : " + id);
        }

        // Method
        public void Identity(int id, String name)
        {

            Console.WriteLine("Name : " + name + ", "
                              + "Id : " + id);
        }
        #endregion
    }
}
