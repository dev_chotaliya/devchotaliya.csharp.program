﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Polimorphism.Dynamic
{
    public class Car
    {
        public virtual string DisplayName()
        {
            return "Car";
        }
    }

    public class TataCar : Car
    {
        public override string DisplayName()
        {
            return "Tata";
        }
    }

    public class MarutiCar : Car
    {
        public override string DisplayName()
        {
            return "Maruti";
        }
    }

}
