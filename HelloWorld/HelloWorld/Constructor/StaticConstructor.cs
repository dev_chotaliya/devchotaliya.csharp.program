﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Constructor
{
    public class StaticConstructor
    {
        // Static constructor
        static StaticConstructor()
        {
            Console.WriteLine("Static Constructor Called");
        }


        // Parametarized constructor
        public StaticConstructor(string value)
        {
            Console.WriteLine("Parametarized Constructor Called");
            Console.WriteLine("Parameter Value:" + value);
        }
    }
}
