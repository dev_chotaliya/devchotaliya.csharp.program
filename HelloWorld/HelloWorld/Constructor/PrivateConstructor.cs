﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Constructor
{
    public class PrivateConstructor
    {
        // Private constructor
        private PrivateConstructor()
        {
            Console.WriteLine("Private Constructor Called");
        }
    }
}
