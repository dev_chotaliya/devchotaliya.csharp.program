﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.Constructor
{
    public class DefaultConstructor
    {
        // Default constructor
        public DefaultConstructor()
        {
            Console.WriteLine("Default Constructor Called");
        }

        // Parametarized constructor
        public DefaultConstructor(string value)
        {
            Console.WriteLine("Parametarized Constructor Called");
            Console.WriteLine("Parameter Value:" + value);
        }
    }
}
