﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.TestInheritence
{
    interface ITestAdd
    {
        void Add();
    }

    interface ITestSub
    {
        void Sub();
    }

    interface ITestMul
    {
        void Mul();
    }

    interface ITestDiv
    {
        void Div();
    }

    public class TestCalc : ITestAdd, ITestSub, ITestMul, ITestDiv
    {
        int a, b, c;

        public TestCalc(int input1, int input2)
        {
            a = input1;
            b = input2;
        }

        public void Add()
        {
            c = a + b;
        }

        public void Div()
        {
            c = a / b;
        }

        public void Mul()
        {
            c = a * b;
        }

        public void Sub()
        {
            c = a - b;
        }

        public int DisplayResult()
        {
            return c;
        }
    }

}
