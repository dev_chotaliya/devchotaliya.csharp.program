﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.TestInheritence
{
    public class TestPrincipal
    {
        public void Monitor()
        {
            Console.WriteLine("Monitor");
        }
    }
    public class TestTeacher : TestPrincipal
    {
        public void Teach()
        {
            Console.WriteLine("Teach");
        }
    }
    public class TestStudent : TestPrincipal
    {
        public void Learn()
        {
            Console.WriteLine("Learn");
        }
    }
}
