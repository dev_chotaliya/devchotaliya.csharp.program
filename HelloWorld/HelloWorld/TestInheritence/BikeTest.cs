﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.TestInheritence
{
    public class BikeTest
    {
        public string DisplayName()
        {
            return "Bike";
        }
    }
    public class ActivaBikeTest : BikeTest
    {
    }

    public class JupiterBikeTest : ActivaBikeTest
    {

    }
}
