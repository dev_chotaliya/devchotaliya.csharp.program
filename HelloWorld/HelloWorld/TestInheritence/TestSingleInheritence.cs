﻿using System;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.TestInheritence
{
    public class TestShape
    {
        public int width;
        public int height;

        public int GetWidth()
        {
            return width;
        }

    }

    public class TestRectangle : TestShape
    {
        public int GetArea()
        {
            return width * height;
        }
    }
}
