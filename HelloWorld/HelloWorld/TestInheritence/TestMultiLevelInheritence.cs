﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.TestInheritence
{
    public class TestGrandFather
    {
        public void DisplayGrandFather()
        {
            Console.WriteLine("GrandFather....");
        }
    }

    public class TestFather : TestGrandFather
    {
        public void DisplayFather()
        {
            Console.WriteLine("Father...");
        }
    }

    public class TestSon : TestFather
    {
        public void DisplaySon()
        {
            Console.WriteLine("Son...");
        }
    }
}
