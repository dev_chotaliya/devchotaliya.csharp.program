﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.TestProperty
{
    public class TestProperty
    {
        private int c;

        public int Result
        {
            get
            {
                return c;
            }
            set
            {
                c = value;
            }
        }

        public int Result1
        {
            get; set;
        }
    }
}
