﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    public class ProtectedAccessModifierBase
    {
        protected int Number1;
    }

    public class ProtectedAccessModifierDerived : ProtectedAccessModifierBase
    {
        public int Get()
        {
            return Number1;
        }
    }

}
