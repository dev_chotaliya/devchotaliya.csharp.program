﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.TestAbstration
{
    public abstract class TestAreaClass
    {
        abstract public int Area(int input);

        public int TestArea(int aa)
        {
            return 0;
        }
    }

    public class TestSquareClass : TestAreaClass
    {
        public override int Area(int input)
        {
            input = input + 1;

            return input * input;
        }
    }
}
